import firebase from 'firebase/app';
import 'firebase/firestore';

const firebaseConfig = {
  apiKey: "AIzaSyB61mZJG4OnJg8zOzm9nAUll7ODaeehEjs",
  authDomain: "sql-demos-fabricio.firebaseapp.com",
  databaseURL: "https://sql-demos-fabricio.firebaseio.com",
  projectId: "sql-demos-fabricio",
  storageBucket: "sql-demos-fabricio.appspot.com",
  messagingSenderId: "900070754872",
  appId: "1:900070754872:web:79c865b5ffd959fe6a34cd"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default firebase.firestore();

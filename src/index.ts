import db from  './firebase/config';
import { FirebaseError } from 'firebase';
import { retornaDocumentos } from './helpers/mostrarDocumentos';

const usuario= {
  nombre:'Santos',
  activo:false,
  fechaNaci:'12-12-1999'
};

/*
db.collection('usuarios')
            .add(usuario)
            .then((docRef)=>{
              console.log(docRef);
            })
            .catch((err)=>{
              console.log('error ',err);
            });
*/

const usuariosRef= db.collection('usuarios');
//Actualizar usuario

/*
usuariosRef.doc('ELN1sB9jy4xz3MAnOj6p').update({
  activo:false,
  fechaNaci:'12-12-1998'
}).then((user)=>{
  console.log('user update  ',user);
}).catch((err:FirebaseError)=>{
  console.log('erro update ',err);
});
*/

//es destructivo, actualiza las propiedades y las que no se manden las elimina y poner nuevos valores
/*
usuariosRef.doc('ELN1sB9jy4xz3MAnOj6p').set({
  activo:false,
  fechaNaci:'12-12-1998',
  casado:true,
  id:123
})
*/

/*
usuariosRef.doc('ELN1sB9jy4xz3MAnOj6p')
            .delete()
            .then( ()=>console.log('Usuario borrado'))
            .catch((err)=>console.log('error ',err));
  */



//seleccionar registros
//se ejecuta cada vez que cambie algo en la base de dato

//usuariosRef.onSnapshot(retornaDocumentos);

//si no me interesa estar pendiente cambios
//usuariosRef.get().then(retornaDocumentos);

//usuariosRef.where('activo','==',true).onSnapshot(retornaDocumentos);

//usuariosRef.where('salario','>',1800).onSnapshot(retornaDocumentos);
/*
usuariosRef.where('salario','>=',1800)
            .where('salario','<=',2000).onSnapshot(retornaDocumentos)

  */

//Consultas compuestas
usuariosRef.where('salario','>=',1800)
            .where('activo','==',true).onSnapshot(retornaDocumentos)













